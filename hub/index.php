<?php
// take data from providers
$provider1Response = file_get_contents('http://localhost:8080/ias/provider1');
$provider2Response = file_get_contents('http://localhost:8080/ias/provider2');

// parse data from JSON to arrays
$cd_new =  json_decode($provider1Response, true);
$cd = json_decode($provider2Response, true);

// transform arrays with data and merge it
$items = array_merge(convertcdnews($cd_new), convertcds($cd));

// make some random order in response
shuffle($items);

// show response in JSON
header('Content-Type: application/json');
echo json_encode($items);



function convertcdnews($cd_new){
    $result = [];
    foreach ($cd_new as $plyta){
        $item = [
            'item_id' => $plyta['id'],
            'tytul' => $plyta['tytul'],
            'autorzy' => $plyta['autorzy'],
            'opis' => $plyta['opis'],
            'sprzedaz' => $plyta['sprzedaz'],
            'utwory' => $plyta['utwory'],
            'wytwornia' => $plyta['wytwornia'],
            'typ' => 'plyta'
        ];
        array_push($result, $item);
    }
    return $result;

}
function convertcds($cd){
    $result = [];
    foreach ($cd as $krazek){
        $item = [
      
            'item_id' => $krazek['id'],
            'tytul' => $krazek['title'],
            'autorzy' => $krazek['authors'],
            'opis' => $krazek['description'],
            'sprzedaz' => $krazek['sold'],
            'utwory' => $krazek['tracks'],
            'wytwornia' => $krazek['works'],
            'typ' => 'krazek'
        ];
        array_push($result, $item);
    }
    return $result;
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

